import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filterbook',
  templateUrl: './filterbook.component.html',
  styleUrls: ['./filterbook.component.css']
})
export class FilterbookComponent implements OnInit {
    @Input() languageOptions;
    @Input() filterBook;
    @Output() optSel = new EventEmitter();
    @Output() optSel1 = new EventEmitter();

  constructor() {}

  ngOnInit() {
    console.log(this.languageOptions);
    console.log(this.filterBook);
  }

  emitChange() {
      this.optSel.emit();
  }

  emitChange1() {
    this.optSel1.emit();
}

}
