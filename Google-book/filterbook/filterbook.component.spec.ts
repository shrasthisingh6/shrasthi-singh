import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterbookComponent } from './filterbook.component';

describe('FilterbookComponent', () => {
  let component: FilterbookComponent;
  let fixture: ComponentFixture<FilterbookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterbookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
