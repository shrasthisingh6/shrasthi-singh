import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { Router } from '@angular/router';
import { AppComponent } from './app.component';
import { PicviewerComponent } from './picviewer/picviewer.component';
import { MyfavComponent } from './myfav/myfav.component';
import { MoviecardComponent } from './moviecard/moviecard.component';
import { BoardComponent } from './board/board.component';
import { SquareComponent } from './square/square.component';
import { NetService } from '@angular/cli/bin/newshrashti/src/app/net.service';
import { HttpClientModule } from '@angular/common/http';
import { HelloComponent } from './hello/hello.component';
import { HomeComponent } from './home/home.component';
import { AddquesComponent } from './addques/addques.component';
import { TeamComponent } from './team/team.component';
import { TeamoptionComponent } from './teamoption/teamoption.component';
import { ShowprodComponent } from './showprod/showprod.component';
import { LogoutComponent } from './logout/logout.component';
import { LeftpanelComponent } from './leftpanel/leftpanel.component';
import { BooksComponent } from './books/books.component';
import { FilterbookComponent } from './filterbook/filterbook.component';
import { GoogleComponent } from './google/google.component';

@NgModule({
  declarations: [
    AppComponent,
    PicviewerComponent,
    MyfavComponent,
    MoviecardComponent,
    BoardComponent,
    SquareComponent,
    HelloComponent,
    HomeComponent,
    AddquesComponent,
    TeamComponent,
    TeamoptionComponent,
    ShowprodComponent,
    LogoutComponent,
    LeftpanelComponent,
    BooksComponent,
    FilterbookComponent,
    GoogleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [NetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
