import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { GoogleComponent } from './google/google.component';


const routes: Routes = [
    {
        path: 'books',
        component : BooksComponent
    },
    {
        path: 'google',
        component: GoogleComponent
    },
    {
        path: '**',
        redirectTo: 'google' , pathMatch: 'full'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
