import { NetService } from '@angular/cli/bin/newshrashti/src/app/net.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
    myurl = 'https://www.googleapis.com/books/v1/volumes?';
    strData: string;
    search1: string;
    title: string;
    flag1 = true;
    flag2 = false;
    max = 8;
    min = 1;
    num = 1;
    language = {option: [{name: 'English', symbol: 'en', isselected: false},
                {name: 'French', symbol: 'fr', isselected: false},
                {name: 'Hindi', symbol: 'hi', isselected: false}], display: 'langRestrict', selected: ''};

    filter1 = {option: [{name: 'Full Volume', symbol: 'full', isselected: false},
                        {name: 'Free Google e-books', symbol: 'free-ebooks', isselected: false},
                        {name: 'Paid Google e-books', symbol: 'paid-ebooks', isselected: false}],
                display: 'filter',
                selected: ''};
    lang1 = null;
    lang = '';
    filt = '';

  constructor(private netService: NetService, private router: Router, private route: ActivatedRoute ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
        return false;
    };
   }

  ngOnInit() {

    this.route.queryParamMap.subscribe(param => {
        this.search1 = param.get('q');
        this.max = param.get('maxResults');
        this.min = param.get('startIndex');
        this.lang = param.get('langRestrict');
        this.filt = param.get('filter');
    }
    );

    this.functionFilter();

    this.netService.getData(this.myurl).subscribe(
        (resp: any) => {
            this.strData = resp.items;
            console.log(this.myurl);
            console.log(this.strData);
            });
  }

  functionFilter() {

      if (this.search1) {
          this.myurl = this.myurl + 'q=' + this.search1 + '&';
      }
      if (this.max) {
          this.myurl = this.myurl + 'maxResults=' + this.max + '&';
      }
      if (this.min) {
          this.myurl = this.myurl + 'startIndex=' + this.min + '&';
    }
      if (this.lang) {
        this.myurl = this.myurl + 'langRestrict=' + this.lang + '&';
    }
      if (this.filt) {
        this.myurl = this.myurl + 'filter=' + this.filt + '&';
    }
  }

  params() {
    const tempArray: any[] = [];
    if (this.search1) {
        const obj1 = {q : this.search1};
        tempArray.push(obj1);
    }
    if (this.max) {
        const obj2 = {maxResults : this.max};
        tempArray.push(obj2);
    }
    if (this.min) {
        const obj3 = {startIndex : this.min};
        tempArray.push(obj3);
    }
    if (this.lang) {
        const obj4 = {langRestrict : this.lang};
        tempArray.push(obj4);
    }
    if (this.filt) {
        const obj5 = {filter : this.filt};
        tempArray.push(obj5);
    }
    const queryParams1 = tempArray.reduce((acc, curr) => Object.assign(acc, curr, {}));
    console.log(tempArray);
    console.log(queryParams1);

    this.router.navigate([/books/], {
        queryParams: queryParams1
    });
}

optChange() {
    let temp = this.language.option.filter(p1 => p1.isselected);
    console.log('language' , temp);
    console.log(this.language);
    let temp1 = temp.map(p1 => {
        return p1.symbol;
    }).join(',');
    console.log(temp1);
    this.lang = temp1;
    this.min = 1;
    this.max = 8;
    this.params();
}

optChange1() {
    let temp = this.filter1.option.filter(p1 => p1.isselected);
    console.log('filter' , temp);
    let temp1 = temp.map(p1 => {
        return p1.symbol;
    }).join(',');
    console.log(temp1);
    this.filt = temp1;
    this.min = 1;
    this.max = 8;
    this.params();
}

goto(x: number) {
    this.num = +this.num + x ;
    this.min = (+this.min ) + (8 * x);
    console.log('num = ' + this.num);
    console.log('max = ' + this.max);
    console.log('min = ' + this.min);
    this.params();
}

}
