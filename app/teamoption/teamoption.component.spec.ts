import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamoptionComponent } from './teamoption.component';

describe('TeamoptionComponent', () => {
  let component: TeamoptionComponent;
  let fixture: ComponentFixture<TeamoptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamoptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamoptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
