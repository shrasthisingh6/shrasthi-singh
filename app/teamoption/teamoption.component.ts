import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-teamoption',
  templateUrl: './teamoption.component.html',
  styleUrls: ['./teamoption.component.css']
})
export class TeamoptionComponent implements OnInit {

  @Input() playersCB;
  @Input() sportsRadio;
  @Output() optSel = new EventEmitter();
  constructor() {}

  ngOnInit() {
    console.log('In teamoption Component');
    console.log('playersCB', this.playersCB);
    console.log('sportsRadio', this.sportsRadio);
  }

  emitChange() {
      this.optSel.emit();
  }

}
