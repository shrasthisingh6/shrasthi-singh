import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Products, prodData} from '../products';

@Component({
  selector: 'app-showprod',
  templateUrl: './showprod.component.html',
  styleUrls: ['./showprod.component.css']
})
export class ShowprodComponent implements OnInit {
    @Input() prod: Products;
    @Output() selProd = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  editProd(i: number) {
      this.selProd.emit(i);
  }

}
