export class Option {
    totalNum: string;
    refineValue: string;
    isSelected: boolean;
}

export class AllOptions {
    options: Option[];
    display: string;
    selected: string;
}
