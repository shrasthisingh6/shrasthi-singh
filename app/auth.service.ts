import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    private loginStatusObs = new Subject<boolean>();
    private loginStatusObs$ = this.loginStatusObs.asObservable();

    loginStatus = false;
    username: string = null;

  constructor() { }

  login(name: string) {
      this.loginStatus = true;
      this.username = name;
      this.loginStatusObs.next(this.loginStatus);
  }

  logout() {
      this.loginStatus = false;
      this.username = null;
      this.loginStatusObs.next(this.loginStatus);
  }
}
