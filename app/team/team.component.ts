import { Component, OnInit } from '@angular/core';
import { Router, ParamMap, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  playersArray = ['Nadal', 'Roger', 'Virat', 'Lionel', 'Jasprit', 'Cristiano'];
  sportsArray = ['Cricket', 'Football', 'Tennis'];
  team = '';
  page = '';
  players = '';
  sport = '';
  playersStructure = null;
  sportsStructure = null;
  playersSelected = '';
  currPage = 1;
  maxPage = 5;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
      this.route.paramMap.subscribe(params => {
          this.team = params.get('team');
      });
      this.route.queryParamMap.subscribe(params => {
          this.page = params.get('page');
          this.players = params.get('players');
          this.sport = params.get('sport');
          this.makeStructure();
      });
  }

  makeStructure() {
      this.currPage = this.page ? +this.page : 1;
      this.playersSelected = this.players ? this.players : '';
      this.playersStructure = this.playersArray.map(pl1 => ({
          name: pl1,
          selected: false
      }));
      let temp1 = this.playersSelected.split(",");
      for (let i=0; i< temp1.length; i++) {
          let item = this.playersStructure.find(p1 => p1.name === temp1[i]);
          if(item) item.selected = true;
      }
      this.sportsStructure = {
          sports: this.sportsArray,
          selected: this.sport ? this.sport : ''
      };
      console.log(this.playersStructure);
      console.log(this.sportsStructure);
  }

  optChange() {
      let temp1 = this.playersStructure.filter(p1 => p1.selected);
      let temp2 = temp1.map(p1 => p1.name);
      this.playersSelected = temp2.join(',');
      let path = '/home/' + this.team;
      let qparams = {};
      if (this.playersSelected) {
      qparams['players'] = this.playersSelected; }
      if (this.sportsStructure.selected) {
      qparams['sport'] = this.sportsStructure.selected; }
      this.router.navigate([path], {queryParams: qparams});
  }

  gotoPage(x: number) {
    this.currPage = this.currPage + x;
    let path = '/home/' + this.team;
    this.router.navigate([path], {
        queryParams: {
            page: this.currPage
        },
        queryParamsHandling: 'merge'
    });
  }

}
