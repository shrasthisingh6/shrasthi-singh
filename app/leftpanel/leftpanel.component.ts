import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Option, AllOptions} from '../options';

@Component({
  selector: 'app-leftpanel',
  templateUrl: './leftpanel.component.html',
  styleUrls: ['./leftpanel.component.css']
})
export class LeftpanelComponent implements OnInit {
    @Input() bestSellerOptions: Option;
    @Input() languageOptions: AllOptions;
    @Output() optSel1 = new EventEmitter();
  constructor() { }

  ngOnInit() {
      console.log(this.bestSellerOptions);
      console.log(this.languageOptions);
  }

  emitChange() {
      this.optSel1.emit();
  }

}
