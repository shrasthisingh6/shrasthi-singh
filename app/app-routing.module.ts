import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HelloComponent} from './hello/hello.component';
import {LogoutComponent } from './logout/logout.component';


const routes: Routes = [
    {
        path: 'hello',
        component : HelloComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'logout',
        component: LogoutComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
