import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Questions } from '../question';

@Component({
  selector: 'app-moviecard',
  templateUrl: './moviecard.component.html',
  styleUrls: ['./moviecard.component.css']
})
export class MoviecardComponent implements OnInit {
  @Input() ques1: Questions;
  @Output() quesChanged = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  selOpt(index: number) {
      console.log(this.ques1.options);
      this.quesChanged.emit();
  }


}
