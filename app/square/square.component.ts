import {Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.css']
})
export class SquareComponent implements OnInit {
 name: string;
 constructor(private route: ActivatedRoute, private router: Router ) {}
 ngOnInit() {
    this.route.paramMap.subscribe(params => {
        this.name = params.get('username')
    });
}

}
