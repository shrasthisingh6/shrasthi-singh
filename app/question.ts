export class Questions {
    text1: string;
    options: string[];
    answer: string;
}

export let quesDB: Questions[] = [
    {text1: 'What is the capital of India', options: ['New Delhi', 'London', 'Paris', 'Tokyo'], answer: 'A'}
];
