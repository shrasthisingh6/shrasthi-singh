import { NetService } from '@angular/cli/bin/newshrashti/src/app/net.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {
    myurl = 'http://localhost:2410/sporticons/stars';
    strData = '';

    constructor(private netService: NetService) {}

    ngOnInit() {
        this.netService.getData(this.myurl).subscribe(
            resp => {
                this.strData = resp;
            }
        );
    }
}
