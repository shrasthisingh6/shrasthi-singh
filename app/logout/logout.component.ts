import { Component, OnInit } from '@angular/core';
import { NetService } from '@angular/cli/bin/newshrashti/src/app/net.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
    myurl = 'http://localhost:2410/booksapp/books';
    strData = '';

  constructor(private netService: NetService) { }

  ngOnInit() {
    this.netService.getData(this.myurl).subscribe(
        (resp: any) => {
            this.strData = resp;
        }
    );
}
  }
