import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Questions, quesDB} from '../question';

@Component({
  selector: 'app-addques',
  templateUrl: './addques.component.html',
  styleUrls: ['./addques.component.css']
})
export class AddquesComponent implements OnInit {
    @Input() ques01: Questions;
    @Output() selQues = new EventEmitter();
    @Output() selQues1 = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  edit() {
      this.selQues.emit();
  }

  deleteQues() {
      this.selQues1.emit();
  }

}
