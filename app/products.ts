export class Products {
    code: string;
    price: number;
    brand: string;
    category: string;
    specialOffer: boolean;
    quantity: number;
    limitedStock: boolean;
}

export let prodData: Products[] = [
    {code: 'PEP1253', price: 20, brand: 'Pepsi', category: 'Food', specialOffer: false, limitedStock: false, quantity: 25},
    {code: 'MAGG021', price: 25, brand: 'Nestle', category: 'Food', specialOffer: true, limitedStock: true, quantity: 10},
    {code: 'LEV501', price:	1000, brand: 'Levis', category: 'Apparel', specialOffer: true, limitedStock: true, quantity: 3},
    {code: 'CLG281', price:	60, brand: 'Colgate', category: 'Personal Care', specialOffer: true, limitedStock: true, quantity: 5},
    {code: 'MAGG451', price: 25, brand:	'Nestle', category: 'Food', specialOffer: true, limitedStock: true, quantity: 0},
    {code: 'PAR250', price:	40,	brand: 'Parachute', category: 'Personal Care', specialOffer: true, limitedStock: true, quantity: 5}
];
