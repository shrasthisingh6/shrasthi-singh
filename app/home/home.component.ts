import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { NetService } from '@angular/cli/bin/newshrashti/src/app/net.service';
import { Component, OnInit } from '@angular/core';
import { Option, AllOptions} from '../options';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    myurl = 'http://localhost:2410/booksapp/books?';
    url = '';
    lowLimit = 1;
    temp = false;
    higherLimit = 10;
    bestSellerOptions1 = [];
    languageOptions1 = [];

    languageOptions: AllOptions = { options : [{ totalNum : '82', refineValue : 'English', isSelected : false},
                                    { totalNum : '16', refineValue : 'French', isSelected : false},
                                    { totalNum : '21', refineValue : 'Latin', isSelected : false},
                                    { totalNum : '27', refineValue : 'Other', isSelected : false}],
                                    display : 'language',
                                    selected : ''};

    bestSellerOptions: AllOptions = { options : [{totalNum: '74', refineValue : 'No', isSelected : false},
                                    { totalNum : '73', refineValue : 'Yes', isSelected : false}],
                                    display : 'bestseller',
                                    selected : ''};
    strData: any = [];
    num: number;
    selopt: string;
    new1: string;
    page: string;
    lang: string = null;
    best: string = null;
  constructor(private netService: NetService, private router: Router, private route: ActivatedRoute ) {
      this.router.routeReuseStrategy.shouldReuseRoute = function() {
          return false;
      };
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe( param => {
        if (this.num === undefined) {
            this.num = 1;
        }
        this.num = param.get('page');
        this.new1 = param.get('newarrival');
        this.lang = param.get('language');
        this.best = param.get('bestSeller');
        console.log(param);
    });

    this.route.paramMap.subscribe( param => {
        this.selopt = param.get('Children');
    });
    this.newArrivalsFun();
    console.log('ngOnInit = ' + this.myurl);

    this.netService.getData(this.myurl).subscribe(
        (resp: any) => {
            this.strData = resp;
            console.log(this.strData);
            });
  }

  newArrivalsFun() {
    if (this.new1) {
           this.myurl = this.myurl + 'newarrival=' + this.new1 + '&';
      }

    if (this.best) {
          this.myurl = this.myurl + 'bestSeller=' + this.best + '&';
      }

    if (this.lang) {
          this.myurl = this.myurl + 'language=' + this.lang + '&';
      }

    if (this.num) {
          this.myurl = this.myurl + 'page=' + this.num + '&';
      }
    console.log(this.myurl);
    }

    params() {
        const tempArray: any[] = [];
        if (this.new1) {
            const obj1 = {newarrival : this.new1};
            tempArray.push(obj1);
        }
        if (this.num) {
            const obj2 = {page: this.num};
            tempArray.push(obj2);
        }
        if (this.best) {
            const obj3 = {bestSeller: this.best};
            tempArray.push(obj3);
        }
        if (this.lang) {
            const obj4 = {language: this.lang};
            tempArray.push(obj4);
        }
        const queryParams1 = tempArray.reduce((acc, curr) => Object.assign(acc, curr, {}));
        console.log(tempArray);
        console.log(queryParams1);

        this.router.navigate([/home/], {
            queryParams: queryParams1
        });
    }

  optChange(index: number) {
      const temp = this.bestSellerOptions.options.filter(p1 => p1.isSelected);
      console.log(temp);
      console.log('bestseller ', this.bestSellerOptions);
      let temp1 = temp.map(p1 => {
          return p1.refineValue; });
      console.log(temp1);
      temp1 = temp1.join(',');
      console.log(temp1);
      this.best = temp1;
      this.num = 1;
      this.params();
  }

  optChange1(index: number) {
    const temp = this.languageOptions.options.filter(p1 => p1.isSelected);
    console.log(temp);
    console.log('language ', this.languageOptions);
    let temp1 = temp.map(p1 => {
        return p1.refineValue; });
    console.log(temp1);
    temp1 = temp1.join(',');
    console.log(temp1);
    this.lang = temp1;
    this.num = 1;
    this.params();
}

  goto(x: number) {
      this.num = +this.num + x ;
      this.lowLimit = this.lowLimit + (x * 10);
      console.log(this.lowLimit);
      this.higherLimit = this.higherLimit + (x * 10);
      console.log(this.higherLimit);
      console.log(this.num);
      this.params();
  }
}
