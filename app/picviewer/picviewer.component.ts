import { Component, Output, Input, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-picviewer',
  templateUrl: './picviewer.component.html',
  styleUrls: ['./picviewer.component.css']
})
export class PicviewerComponent implements OnInit {
  @Input() namesRadio;
  @Output() optSel = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  emitChange() {
      this.optSel.emit();
  }
}
