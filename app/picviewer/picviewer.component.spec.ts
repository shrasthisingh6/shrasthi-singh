import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicviewerComponent } from './picviewer.component';

describe('PicviewerComponent', () => {
  let component: PicviewerComponent;
  let fixture: ComponentFixture<PicviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
