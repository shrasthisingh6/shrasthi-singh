import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-myfav',
  templateUrl: './myfav.component.html',
  styleUrls: ['./myfav.component.css']
})
export class MyfavComponent implements OnInit {

names = ['Jack', 'Steve', 'William', 'Kathy', 'Edward'];
namesIds = [
    { name: 'Jack', id: 'AB455'},
    { name: 'Steve', id: 'GM072'},
    { name: 'William', id: 'CX499'},
    { name: 'Kathy', id: 'MM746'},
    {name: 'Edward', id: 'KT108'}
];
  namesRadioStructure = null;
  nameRadioSelected = "";

  ngOnInit() {
      this.updateValue();
  }

  updateValue() {
      this.namesRadioStructure = {
        name: this.names,
        selected: ""
      };
  }

  optChange() {
      console.log(this.namesRadioStructure);
      this.nameRadioSelected= this.namesRadioStructure.selected;
  }

}
