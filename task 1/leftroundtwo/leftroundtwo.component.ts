import { Component, OnInit, Output, Input, EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-leftroundtwo',
  templateUrl: './leftroundtwo.component.html',
  styleUrls: ['./leftroundtwo.component.css']
})
export class LeftroundtwoComponent implements OnInit {
  @Input() bestSellerOptions;
  @Output() optSel1 = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  emitChange() {
    this.optSel1.emit();
}

}
