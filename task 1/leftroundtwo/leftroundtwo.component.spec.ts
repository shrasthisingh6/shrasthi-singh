import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftroundtwoComponent } from './leftroundtwo.component';

describe('LeftroundtwoComponent', () => {
  let component: LeftroundtwoComponent;
  let fixture: ComponentFixture<LeftroundtwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftroundtwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftroundtwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
