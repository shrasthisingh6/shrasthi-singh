import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-leftroundone',
  templateUrl: './leftroundone.component.html',
  styleUrls: ['./leftroundone.component.css']
})
export class LeftroundoneComponent implements OnInit {
  @Input() bestSellerOptions;
  @Output() optSel1 = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.bestSellerOptions);
  }

  emitChange() {
    this.optSel1.emit();
}

}
