import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftroundoneComponent } from './leftroundone.component';

describe('LeftroundoneComponent', () => {
  let component: LeftroundoneComponent;
  let fixture: ComponentFixture<LeftroundoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftroundoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftroundoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
