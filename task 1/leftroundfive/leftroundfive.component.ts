import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-leftroundfive',
  templateUrl: './leftroundfive.component.html',
  styleUrls: ['./leftroundfive.component.css']
})
export class LeftroundfiveComponent implements OnInit {
  @Input() bestSellerOptions;
  @Output() optSel1 = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.bestSellerOptions);
  }

  emitChange() {
    this.optSel1.emit();
}

}
