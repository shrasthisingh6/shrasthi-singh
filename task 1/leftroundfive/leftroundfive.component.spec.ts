import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftroundfiveComponent } from './leftroundfive.component';

describe('LeftroundfiveComponent', () => {
  let component: LeftroundfiveComponent;
  let fixture: ComponentFixture<LeftroundfiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftroundfiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftroundfiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
