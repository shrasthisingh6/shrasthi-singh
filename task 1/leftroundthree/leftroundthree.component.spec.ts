import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftroundthreeComponent } from './leftroundthree.component';

describe('LeftroundthreeComponent', () => {
  let component: LeftroundthreeComponent;
  let fixture: ComponentFixture<LeftroundthreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftroundthreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftroundthreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
