import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-leftroundthree',
  templateUrl: './leftroundthree.component.html',
  styleUrls: ['./leftroundthree.component.css']
})
export class LeftroundthreeComponent implements OnInit {
  @Input() bestSellerOptions;
  @Output() optSel1 = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.bestSellerOptions);
  }

  emitChange() {
    this.optSel1.emit();
}

}
