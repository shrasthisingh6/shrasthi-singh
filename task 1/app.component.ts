import { Component, OnInit } from '@angular/core';
import { NetService } from '@angular/cli/bin/projectbooksnew/src/app/net.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  url = 'http://localhost:2410/carshopes';
  strdata: any;
  strdata2: any;
  strdata3: any;
  strdata4: any;
  strdata5: any;

  constructor(private netservice: NetService) {}

  ngOnInit() {
    this.netservice.getData(this.url).subscribe((resp: any) => {
      this.strdata = resp;
      console.log(this.strdata);
    });
  }

  change1() {
    let temp1 = this.strdata.filter(p1 => p1.isSelected);
    if (temp1.length <= 8) {
    console.log(temp1);
    this.strdata2 = temp1.map(p1 => ({
      id: p1.id,
      name: p1.name,
      gender: p1.gender,
      weight: p1.weight,
      isSelected: false
    }));
    console.log(this.strdata2);}
  }
  change2() {
    let temp1 = this.strdata2.filter(p1 => p1.isSelected);
    console.log(temp1);
    if (temp1.length <= 4) {
    this.strdata3 = temp1.map(p1 => ({
      id: p1.id,
      name: p1.name,
      gender: p1.gender,
      weight: p1.weight,
      isSelected: false
    }));
    console.log(this.strdata3);}
  }
  change3() {
    let temp1 = this.strdata3.filter(p1 => p1.isSelected);
    console.log(temp1);
    if (temp1.length <= 2) {
    this.strdata4 = temp1.map(p1 => ({
      id: p1.id,
      name: p1.name,
      gender: p1.gender,
      weight: p1.weight,
      isSelected: false
    }));
    console.log(this.strdata4);}
  }
  change4() {
    let temp1 = this.strdata4.filter(p1 => p1.isSelected);
    console.log(temp1);
    if (temp1.length <= 1) {
    this.strdata5 = temp1.map(p1 => ({
      id: p1.id,
      name: p1.name,
      gender: p1.gender,
      weight: p1.weight,
      isSelected: false
    }));
    console.log(this.strdata5);}
  }
  change5() {
    let temp1 = this.strdata5.filter(p1 => p1.isSelected);
    console.log(temp1);
    let strdata6 = temp1.map(p1 => ({
      id: p1.id,
      name: p1.name,
      gender: p1.gender,
      weight: p1.weight,
      isSelected: false
    }));
    console.log(strdata6);
  }
}
