import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftroundfourComponent } from './leftroundfour.component';

describe('LeftroundfourComponent', () => {
  let component: LeftroundfourComponent;
  let fixture: ComponentFixture<LeftroundfourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftroundfourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftroundfourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
