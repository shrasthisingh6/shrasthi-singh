import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-leftroundfour',
  templateUrl: './leftroundfour.component.html',
  styleUrls: ['./leftroundfour.component.css']
})
export class LeftroundfourComponent implements OnInit {
  @Input() bestSellerOptions;
  @Output() optSel1 = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.bestSellerOptions);
  }

  emitChange() {
    this.optSel1.emit();
}

}
