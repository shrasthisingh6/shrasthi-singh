var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});
const port = 2410;
app.listen(port, () => console.log("listening on port : ", port));

players = [{id: 1, name: 'Latika Bhandari', gender: 'Female', weight: '53kg', isSelected: false},
            {id: 2, name: 'Rodali Barua', gender: 'Female', weight: '73kg', isSelected: false},
            {id: 3, name: 'Aman Kumar', gender: 'Male', weight: '54kg', isSelected: false},
            {id: 4, name: 'Kashish Malik', gender: 'Female', weight: '57kg', isSelected: false},
            {id: 5, name: 'Saurav', gender: 'Male', weight: '63kg', isSelected: false},
            {id: 6, name: 'Kanha Mainali', gender: 'Male', weight: '54kg', isSelected: false},
            {id: 7, name: 'Navjeet Maan', gender: 'Male', weight: '80kg', isSelected: false},
            {id: 8, name: 'Sonam Rawal', gender: 'Female', weight: '62kg', isSelected: false},
            {id: 9, name: 'Margerette Maria Regi', gender: 'Female', weight: '73kg', isSelected: false},
            {id: 10, name: 'Gajendra Parihar', gender: 'Male', weight: '58kg', isSelected: false},
            {id: 11, name: 'Akshay kumar', gender: 'Male', weight: '87kg', isSelected: false},
            {id: 12, name: 'Sabita Ramchiary', gender: 'Female', weight: '57kg', isSelected: false},
            {id: 13, name: 'Naveen', gender: 'Male', weight: '68kg', isSelected: false},
            {id: 14, name: 'Shivansh Tyagi', gender: 'Male', weight: '74kg', isSelected: false},
            {id: 15, name: 'Purva Dixit', gender: 'Female', weight: '49kg', isSelected: false},
            {id: 16, name: 'Aishwayra R. Ravade', gender: 'Female', weight: '73kg', isSelected: false}];


app.get("/carshopes", function(req, res) {
  outArr = players;
  res.send(outArr);
});



